"""
lsystem.py

cs1120 Spring 2016
Project 3

This file provides definitions of functions for creating L-System commands.

You will need to complete some of the definitions for Exercuse 1, but do
not need to turn in this file.

You should not need to modify anything else in this file, but should 
understand everything in it.
"""

# Functions for making L-System Commands

def make_forward_command():
    """Returns a representation of a Forward command."""
    return ["F"]

def make_rotate_command(angle):
    """Returns a representation of a Rotate command with angle given by the input."""
    return ["R", angle]

def make_offshoot_command(commands):
    """Returns a representation of an Offshoot command, with input commands as the offshoot."""
    return ["O", commands]

def make_lsystem_sequence(*args):
    """Returns a representation of an L-System command sequence."""
    # The *args means take any number of inputs as a list.
    return list(args)

# Functions for manipulating L-System Commands

## Complete these for Exercise 1.

def is_forward(lcommand):
    """
    # is_forward(lcommand) evaluates to True if lcommand is a forward
    # command (indicated by its first element being the 'F' tag)
    """
    return lcommand[0] == 'F'

def is_rotate(lcommand):
    """
    # is_rotate(lcommand) evaluates to True if lcommand is a rotate
    # command (indicated by its first element being the 'R' tag)
    """
    return lcommand[0] == 'R'

def is_offshoot(lcommand):
    """
    # is_offshoot(lcommand) evaluates to True if lcommand is an offshoot
    # command (indicated by its first element being the 'O' tag)
    """
    return lcommand[0] == 'O'

def get_angle(lcommand):
    """
    # get_angle() evaluates to the angle associated with the rotate
    # command. Produces an error if the command is not a rotate command.
    """
    if is_rotate(lcommand):
        return lcommand[1]  
    else:
        # This next line produces an error and prints out the
        # message 'Not a rotate command'. We'll cover the Python
        # keyword "raise" later in class. For now it just means
        # "stop and signal an error".
        raise RuntimeError('Not a rotate command')

def get_offshoot_commands(lcommand):
    """
        # get_offshoot_commands(lcommand) evaluates to the offshoot command
        # list associated with lcommand. You should produce an
        # error (as above) if lcommand is not an offshoot command.
    """
    if is_offshoot(lcommand):
        assert len(lcommand) == 2
        return lcommand[1]
    else:
        return RuntimeError('Not a offshoot command')

### 
### End of Exercise 1
###
### You will not need to change anything below here, but should read through
### and understand all of this code.
###

def is_lsystem_command(lcommand):
    """
    Returns True if the input is an L-System command; False otherwise.
    """
    # Once you have defined is_forward, is_rotate and is_offshot above,
    # this is_lsystem_command should work perfectly with no changes.
    return is_forward(lcommand) or is_rotate(lcommand) or is_offshoot(lcommand)

### To make debugging easier, here are some functions for converting
### L-System commands into easier to read strings (unparsing) and
### producing L-System commands from strings (parsing).
###
### (Note for black belts: this would be a lot better if we used
### a class, and then could override the __str__ methods.  But,
### we don't want to deal with classes yet.)

def unparse_lsystem_command(command):
    """Returns a string representation of the L-System command."""
    assert is_lsystem_command(command)
    if is_forward(command):
        return "F"
    elif is_rotate(command):
        return "R" + str(get_angle(command))
    elif is_offshoot(command):
        return "O"  + unparse_lsystem_sequence(get_offshoot_commands(command))
    else:
        raise RuntimeError("Blimey! Attempt to unparse non-lsystem command: " + str(command))

def unparse_lsystem_sequence(commands):
    """Returns a string representing the L-System command sequence."""
    return '[' + ', '.join([unparse_lsystem_command(command) for command in commands]) + ']'

def parse_lsystem_command(command):
    """Returns an L-System command corresponding to the input string."""
    if command == 'F':
        return make_forward_command()
    elif command[0] == 'R':
        return make_rotate_command(int(command[1:]))
    elif command[0] == 'O':
        return make_offshoot_command(parse_lsystem_sequence(command[1:]))
    else:
        raise RuntimeError("Houston we have a problem: " + str(command))

def parse_lsystem_sequence(sequence):
    """Returns an L-System command sequence (list of L-System commands) corresponding to the input string."""
    from string import whitespace
    # This is very fragile parsing code, but seems to work for our (non-evil) purposes.
    # This is extremely ugly code, though, and anyone who substantially improves it
    # deserves an automatic belt promotion!

    # must start and end with ['s, which we remove
    assert sequence[0] == '['
    assert sequence[-1] == ']'

    # Unfortunately, this doesn't work because of O syntax
    # [parse_lsystem_command(cmd.strip()) for cmd in sequence[1:-1].split(',')]
    guts = sequence[1:-1]
    commands = []
    command = ''
    i = 0
    while i < len(guts):
        c = guts[i]
        if c == 'O': # offshoot command, need to parse inner sequence
            assert not command
            i = i + 1
            startindex = i
            endindex = None
            assert guts[i] == '['
            nesting = 0
            while i < len(guts):
                if guts[i] == ']':
                    nesting = nesting - 1
                    if nesting == 0:
                        endindex = i
                        break
                elif guts[i] == '[':
                    nesting = nesting + 1
                else:
                    pass
                i = i + 1
            assert endindex
            seq = guts[startindex:endindex + 1]
            ocommand = make_offshoot_command(parse_lsystem_sequence(seq))
            commands.append(ocommand)
            i = endindex + 2
        else:
            if c == ',':
                commands.append(parse_lsystem_command(command))
                command = ''
            else:
                if c in whitespace: 
                    pass
                else:
                    command = command + c
            i = i + 1

    if command:
        commands.append(parse_lsystem_command(command))

    return commands

# L-System commands for the tree fractal
# This is the replacement
# F ::= [F, O[R30 F], F, O[R-60, F], F]

TREE_COMMANDS = parse_lsystem_sequence('[F, O[R30, F], F, O[R-60, F], F]')
